-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27-Abr-2019 às 21:12
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_felipe`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ebook`
--
CREATE DATABASE [IF NOT EXISTS] lp2_felipe;


CREATE TABLE `ebook` (
  `id` varchar(30) NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `img` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `visualizacoes` int(3) DEFAULT NULL,
  `alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ebook`
--

INSERT INTO `ebook` (`id`, `titulo`, `img`, `link`, `visualizacoes`, `alteracao`) VALUES
('12WUh4z1_UgJa7ABZdQLqP4cellWnG', 'Cosmos DB for Mongo DB Develop', 'https://lh3.googleusercontent.com/dh0EKOAL6xGpyWWMjkw0kTAl7-7bYy90jH1o8fiETerAXg9ubHqCpMolYRJqet6CpUjDDuAkszc=s220', 'https://drive.google.com/file/d/12WUh4z1_UgJa7ABZdQLqP4cellWnGL6I/view?usp=drivesdk', 1, '2019-04-27 16:33:53'),
('17JA5xI7Bw_QG5Adxk2CUK0pLL8fYR', 'Pro C#7', 'https://lh3.googleusercontent.com/hPdWmhEYHPYDbCQdFsA7eqqjqfsUp-jGtR7-st0BoVOCP8NCtTC3eOkjJcSFQXgGvU0ywv8jc4c=s220', 'https://drive.google.com/file/d/17JA5xI7Bw_QG5Adxk2CUK0pLL8fYRrTH/view?usp=drivesdk', 2, '2019-04-27 16:33:53'),
('1cX2lvdRpyAsUUA-fl9DLScMZos6CG', 'C#7 Quick Syntax Reference', 'https://lh3.googleusercontent.com/Ru87GvMGi48fq5190Y7tgW14ifDNms1ulgBtVo1-GvQDabydOak9Jz2nEixNs7hWNE2B3vrAMQU=s220', 'https://drive.google.com/file/d/1cX2lvdRpyAsUUA-fl9DLScMZos6CGwNz/view?usp=drivesdk', 1, '2019-04-27 16:33:53'),
('1EOfm_CYPnCKkSRLa0f0MVVcJdeJ9h', 'Modern Data Access with Entity', 'https://lh3.googleusercontent.com/9kfbarjD5w6knongSFMnBEdpKsq6LK2d4HGn59Q6N-bAZn6vu57SyVMJlfdPfZIGxFl-Uw3Dss4=s220', 'https://drive.google.com/file/d/1EOfm_CYPnCKkSRLa0f0MVVcJdeJ9hShe/view?usp=drivesdk', 1, '2019-04-27 16:33:53'),
('1HY17uejHVfNCTtRdPtLyMdQNBiGQm', 'Azure and Xamarin Forms', 'https://lh3.googleusercontent.com/Ec4nYZSB4lC3c8abJ7wdpmknpB1Vq-gUefh9_4eIPU-PyI22ZPwtScB_NB7GnK5YdYyE9xqsvCs=s220', 'https://drive.google.com/file/d/1HY17uejHVfNCTtRdPtLyMdQNBiGQmScA/view?usp=drivesdk', 2, '2019-04-27 16:33:53'),
('1jeazMj0HiB9RdwtfU2RyuzAnu1AKO', 'Developing Bots with Microsoft', 'https://lh3.googleusercontent.com/IPZpl4MU63cTDLXq2ncJYHWOHKQ_4ZdTZpfkAFdAXlz3m0tYt9Su1fowFNyT69CcM0xL_fVnzvk=s220', 'https://drive.google.com/file/d/1jeazMj0HiB9RdwtfU2RyuzAnu1AKOX52/view?usp=drivesdk', NULL, '2019-04-27 16:33:53'),
('1JmKQn1fUqasBbUwgJQTuy3p8rqk1q', 'Modern API Design with Asp,Net', 'https://lh3.googleusercontent.com/X3lNv1UXaUxga_QFtI7VDqZe_xNsy0H2e8v8mmhp8Vjf5AAoOjG0vc6zGKNl0ApopmFf5_dFDjw=s220', 'https://drive.google.com/file/d/1JmKQn1fUqasBbUwgJQTuy3p8rqk1qr68/view?usp=drivesdk', 2, '2019-04-27 16:33:53'),
('1L7hZFvgpUPuD3P0YSD_pnEYSH45-z', 'Essential Docker for Asp.Net C', 'https://lh3.googleusercontent.com/Ncx2JRAevRnt1Ykd0YmbRxwFVoPD5I68HU7f3WKXXN8PfS9X0F7wTwWziMD_APCFCSt1pQKsMYA=s220', 'https://drive.google.com/file/d/1L7hZFvgpUPuD3P0YSD_pnEYSH45-zIZe/view?usp=drivesdk', 1, '2019-04-27 16:33:53'),
('1mea8ATNkqSdVxkMSK1M7Ohmkcwvrn', 'Beginning Entity Framework Cor', 'https://lh3.googleusercontent.com/JrBLi7McTtOni6TKTDtIoREd7KxwzDkEU58p1uOtmBxO83CX8SnUhzg_u4uXOMfCOCZr7WqW64g=s220', 'https://drive.google.com/file/d/1mea8ATNkqSdVxkMSK1M7OhmkcwvrnUXv/view?usp=drivesdk', NULL, '2019-04-27 16:33:53'),
('1Q0K4WZNpqp8XAjV6W_Lud3vqeHI5e', 'Beginning Xamarin Development ', 'https://lh3.googleusercontent.com/bhRXNOf469pMGftVGduqilrzp1MbTUYbQx_83qhu-PU11UNYHAMFDRwmDY8IA0IJ27bGTvyp6vs=s220', 'https://drive.google.com/file/d/1Q0K4WZNpqp8XAjV6W_Lud3vqeHI5eLsA/view?usp=drivesdk', 3, '2019-04-27 16:33:53'),
('1w0e5WlP1GdKzCgEM7c1HQ_7cqRDjT', 'Visual Studio Code Distilled', 'https://lh3.googleusercontent.com/LY5z8M5OfnuXh8n2xJxXcr1VookoHIvF7-3JqIfIEQmaeycQHGHKchAWNDnwPfputubvfaR9Eh8=s220', 'https://drive.google.com/file/d/1w0e5WlP1GdKzCgEM7c1HQ_7cqRDjTtr2/view?usp=drivesdk', 1, '2019-04-27 16:33:53'),
('1Yabk_rPEYboQSRfulZtVDN3K4YmGg', 'Microsoft Computer Vision APIS', 'https://lh3.googleusercontent.com/obhfC7G_jlLQz7yP20cwoqIBUDqz5oInugG7LTWxomUX44FRr1CEW2bO6lGWu3VFtMOD2QzS7vQ=s220', 'https://drive.google.com/file/d/1Yabk_rPEYboQSRfulZtVDN3K4YmGgi18/view?usp=drivesdk', 2, '2019-04-27 16:33:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ebook`
--
ALTER TABLE `ebook`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
