$(".perguntas").hover(function()
  {
    $(this).removeClass("grey lighten-3");
    $(this).addClass("light-green lighten-1")
  }, function(){
    $(this).removeClass("light-green lighten-1");
    $(this).addClass("grey lighten-3")

});

$("#pergunta1").click(function(){
    $("#resposta1").css('display', 'block')
    $("#resposta2").css('display', 'none')
    $("#resposta3").css('display', 'none')
    $("#resposta4").css('display', 'none')  
});

$("#pergunta2").click(function(){
    $("#resposta1").css('display', 'none')
    $("#resposta2").css('display', 'block')
    $("#resposta3").css('display', 'none')
    $("#resposta4").css('display', 'none') 
});

$("#pergunta3").click(function(){
    $("#resposta1").css('display', 'none')
    $("#resposta2").css('display', 'none')
    $("#resposta3").css('display', 'block')
    $("#resposta4").css('display', 'none')  
});

$("#pergunta4").click(function(){
    $("#resposta1").css('display', 'none')
    $("#resposta2").css('display', 'none')
    $("#resposta3").css('display', 'none')
    $("#resposta4").css('display', 'block') 
});
