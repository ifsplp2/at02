<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
    public function index()
    {
        $this->load->view('common/Header');
        $this->load->view('common/NavBar');

        $v['Titulo'] = "Google&nbsp Drive&nbsp Api";
        $this->load->view('Home/Index', $v);

        $this->load->view('common/Footer');
    }

    public function api()
    {
        $this->load->view('common/Header');
        $this->load->view('common/NavBar');
        
        $v['Resposta1'] = $this->load->view('Home/Resposta1', null, true);
        $v['Resposta2'] = $this->load->view('Home/Resposta2', null, true);
        $v['Resposta3'] = $this->load->view('Home/Resposta3', null, true);
        $v['Resposta4'] = $this->load->view('Home/Resposta4', null, true);
        $this->load->view('Home/Api', $v);

        $this->load->view('common/Footer');
    }
}