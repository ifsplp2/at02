<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biblioteca extends CI_Controller 
{
    public function index()
    {
        $this->load->view('common/Header');
        $this->load->view('common/NavBar');       

        $this->load->model('BibliotecaModel');
        $this->load->model('ComponentModel');
        $this->BibliotecaModel->updateDatabase();

        $lista = $this->BibliotecaModel->getAll();
        
        $v ['ebookCard'] = $this->ComponentModel->ebookCard($lista, count($lista));
        $v ['carousel'] = $this->ComponentModel->carousel(base_url("assets/img/carousel_1.png"), base_url("assets/img/carousel_2.png"), base_url("assets/img/carousel_3.png"));                 

        $this->load->view('Biblioteca/Index', $v);
                
        $this->load->view('common/Footer');
    }

    public function log()
    {
        $this->load->view('common/Header');
        $this->load->view('common/NavBar');      
        
        $this->load->model('ContadorModel');
        $this->load->model('ComponentModel');

        $ebooks = $this->ContadorModel->getAll();
        $v['table'] = $this->ComponentModel->table($ebooks);
        $v['total'] = $this->ContadorModel->totalAcessos();

        $this->load->view('Biblioteca/Log', $v);
                
        $this->load->view('common/Footer');
    }

    public function visualizarLivro($link)
    {
        $link = str_replace('traco2019', '/', $link);
        $this->load->helper('url'); 

        $this->load->model('ContadorModel');
        $val = $this->ContadorModel->updateContador($link);

        redirect($link);
    }
}