<div class="container">

    <div class="mt-4 p-2">
        <?=$carousel?>
    </div>

    <div class="p-3 text-center light-green lighten-1">
        <h4>Novos Titulos todo mês</h4>
    </div>

    <div class="row mt-4 p-3">
        <?=$ebookCard?>
    </div>
</div>