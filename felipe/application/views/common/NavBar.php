<nav class="navbar navbar-expand-lg navbar-dark teal darken-4">
    <a class="navbar-brand mr-5 font-weight-bold" href="<?= base_url()?>">LP2</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url()?>Home/api">API <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url()?>Biblioteca">Biblioteca</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url()?>Biblioteca/log">Log</a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto nav-flex-icons">
            <li class="nav-item avatar">
                <a class="nav-link p-0" href="http://portal.ifspguarulhos.edu.br/" target="_blank">
                    <img src="<?= base_url()?>assets\img\if_logo.png" class="rounded-circle z-depth-0"
                        alt="avatar image" height="35">
                </a>
            </li>
        </ul>
    </div>
</nav>