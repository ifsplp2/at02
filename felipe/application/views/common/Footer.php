<footer class="page-footer font-small teal darken-4">

    <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/">gu3002284</a>
    </div>

</footer>

<script type="text/javascript" src="<?= base_url() ?>assets/mdb/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/mdb/js/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/mdb/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/mdb/js/mdb.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/javascript/perguntas.js"></script>

</body>

</html>