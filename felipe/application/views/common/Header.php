<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Google Drive - API</title>

    <link rel="icon" type="imagem/png" href="<?= base_url()?>assets/img/icon_book.png" />

    <link href="<?= base_url() ?>assets/mdb/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/mdb/css/mdb.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/mdb/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/customCss.css" rel="stylesheet">

    
    <link href="https://fonts.googleapis.com/css?family=Barrio" rel="stylesheet">
</head>

<body>