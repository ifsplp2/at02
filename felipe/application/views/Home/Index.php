<div class="container mt-4 mb-4">
    <div class="jumbotron jumbotron-fluid grey lighten-3">
        <div class="container">
            <h1 class="display-4 text-center teal-text fontBarrio"><?=$Titulo?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-8 d-flex align-items-center">
            <div class="card grey lighten-3 p-2">
                <div class="row">
                    <div class="col-4 d-flex justify-content-center view overlay zoom">
                        <img class="m-2 img-fluid" src="<?= base_url()?>assets/img/foto_felipe.jpg"
                            alt="foto desenvolvedor">
                    </div>

                    <div class="col-8 p-2 d-flex align-items-center">
                        <div>
                            <p><span class="font-weight-bold teal-text">Nome:</span> Felipe P. Claro</p>
                            <p><span class="font-weight-bold teal-text">Matricula:</span> GU3002284</p>
                            <p><span class="font-weight-bold teal-text">Curso:</span> Análise e Desenvolvimento de
                                Sistemas</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <img class="m-2 p-3 img-fluid" src="<?= base_url()?>assets/img/gd_logo.png" alt="logo Google Drive">
        </div>
    </div>

</div>