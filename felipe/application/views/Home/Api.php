<div class="container-fluid bg-img-gd mt-4 mb-4">

    <div class="container">
        <div class="jumbotron jumbotron-fluid grey lighten-3">
            <div class="container">
                <h1 class="display-4 text-center teal-text fontBarrio">Sobre a API...</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 p-5">
                <div class="card mb-4 grey lighten-3 p-2 perguntas" id="pergunta1">
                    <h4><span class="font-weight-bold teal-text">1.</span> Quais são as principais características da API e suas finalidades?</h4>
                </div>
                <div class="card mb-4 grey lighten-3 p-2 perguntas" id="pergunta2">
                    <h4><span class="font-weight-bold teal-text">2.</span> O que é possível criar com o uso desta API?</h4>
                </div>
                <div class="card mb-4 grey lighten-3 p-2 perguntas" id="pergunta3">
                    <h4><span class="font-weight-bold teal-text">3.</span> Quais são as restrições para o uso da API? Tem custo? Tem boa documentação? E outras...</h4>
                </div>
                <div class="card grey lighten-3 p-2 perguntas" id="pergunta4">
                    <h4><span class="font-weight-bold teal-text">4.</span> Quais foram os passos necessários para a implementação da sua aplicação com o uso desta API?
                    </h4>
                </div>
            </div>
            <div class="col-12 col-lg-6 p-5">
                <div class="card grey lighten-3 p-2 quadro-resposta">
                    <?=$Resposta1?>
                    <?=$Resposta2?>
                    <?=$Resposta3?>
                    <?=$Resposta4?>
                </div>
            </div>
        </div>
    </div>
</div>