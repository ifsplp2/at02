<div class="hide mt-3" id="resposta4">
    <p>
        &nbsp&nbsp&nbsp&nbsp&nbspO uso mais comum da API é para acesso do usuário ao seu próprio Google Drive,
        entretanto para fins acadêmicos optei por fazer um repositório compartilhado de E-books, simulando um banco de
        arquivos na nuvem,
        onde o usuário terá acesso a um acervo gerenciado pelo administrador do site.

    </p>

    <p>
        Os principais passos para a criação de aplicação foram:
    </p>

    <p>
        <span>1</span> - Criação de conta no gmail: Para uso do Google Drive propriamente dito e do painel de controle
        de API's da
        google.
    </p>

    <p>
        2 - Enable drive api no painel google e Criação de Credentials e Token de acesso no formato Json:
    </p>

    <p>
        3 - Instalar Composer no diretorio do projeto: A google conta com uma library propria para uso de suas API's,
        e o composer facilita sua instalação e roteamento no projeto.
    </p>

    <p>
        4 - Criar classe para conectar a API, com métodos para a manipulação de dados: Foi necessario criar uma classe
        que
        faz a conexão com API,
        definindo parametros e permissões sobre o Google Drive conectado, bem como metodos para tratamento das
        informações
        recebidas em Json.
    </p>
</div>