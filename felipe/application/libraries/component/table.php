<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class table
{
    private $ebooks;

    function __construct($ebooks)
    {
        $this->ebooks = $ebooks;
    }

    public function getHTML()
    {
        $html = '
        <table id="dtBasicExample" class="table table-striped display responsive nowrap" style="width:100%">
        <thead>
          <tr>
              <th class="th-sm text-center" data-priority="1">Titulo
              </th>  
              <th class="th-sm text-center">Visualizações
              </th>     
              <th class="th-sm text-center">Modificado em
              </th>
          </tr>
        </thead>
        <tbody>     
          
        ';
        foreach($this->ebooks as $ebook)
        {
            $html .= '      
            <tr>         
                <td>
                    '.$ebook->getTitulo().'
                </td>
                <td class="text-center">
                    '.$ebook->getVisualizacoes().' 
                </td>  
                <td class="text-center">
                    '.$ebook->getAlteracao().'
                </td>                        
            </tr>';        
        }

        $html .= '
        </tbody> 
        </table>';
           
        return $html;
    }
}

