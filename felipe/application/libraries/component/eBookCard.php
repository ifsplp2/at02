<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class eBookCard
{
    private $titulo;
    private $img;
    private $link;

    function __construct($ebook)
    {
        $this->titulo = $ebook->getTitulo();
        $this->img = $ebook->getImg();
        $this->link = $ebook->getLink();
    }

    public function getHTML()
    {
        $url = "Biblioteca/visualizarLivro/".str_replace('/', 'traco2019', $this->link);
        
        $html = 
        '
        <div class="card col-md-4 mb-4 p-4 grey lighten-3">
            

            <img class="card-img-top card-small" src="'.$this->img.'" alt="Card image cap"/>

            
            <div class="card-body">              
                
                <p class="card-text">'.$this->titulo.'</p>
                    
                <a href="'.base_url($url).'" class="btn btn-primary light-green lighten-1">Visualizar</a>

            </div>
           
        </div> ';

        return $html;
    }
}