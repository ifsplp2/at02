<?php
defined('BASEPATH') or exit('No direct script access allowed');

class bibliotecaCarousel
{
    private $img1;
    private $img2;
    private $img3;

    public function __construct($img1, $img2, $img3)
    {
        $this->img1 = $img1;
        $this->img2 = $img2;
        $this->img3 = $img3;
    }

    public function getHTML()
    {
        $html =
            '
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" interval="2000">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="'.$this->img1.'">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="'.$this->img2.'">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="'.$this->img3.'">
                </div>
            </div>
        </div>';

        return $html;
    }
}