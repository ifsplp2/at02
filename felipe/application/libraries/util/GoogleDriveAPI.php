<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';

class GoogleDriveAPI
{
    private $gdClient;
    private $gdService;

    function __construct()
    {
        $this->gdClient = $this->getClient();
        $this->gdService = new Google_Service_Drive($this->gdClient);
    }

    public function getAll()
    {          
        $optParams = array(
        'pageSize' => 20,
        'fields' => 'nextPageToken, files(id, name, webViewLink, thumbnailLink)'
        );

        return $results = $this->gdService->files->listFiles( $optParams);  
    }

    public function openFile($fileId)
    {         
        $optParams = array(
            
            'fields' => 'id, name, webViewLink, thumbnailLink');
                
        $response = $this->gdService->files->get($fileId, $optParams);
        
        //var_dump($response->getWebViewLink());
    }

    private function getClient()
    {       
        $client = new Google_Client();
        $client->setScopes(Google_Service_Drive::DRIVE);
        $client->setAuthConfig('credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        
        $tokenPath = 'token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }
}