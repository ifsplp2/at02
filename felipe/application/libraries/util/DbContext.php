<?php

class DbContext
{
    private $db;
    private $table;

    function __construct($table)
    {
        $ci = & get_instance();
        $this->db = $ci->db;
        $this->table = $table;
    }

    public function getAll()
    {
        $rs = $this->db->get($this->table);

        return $rs->result_array();
    }

    public function getFrom($coluna, $value)
    {
        $sql = "SELECT * FROM $this->table WHERE $coluna = '$value'";

        $res = $this->db->query($sql);

        $data = $res->result_array();

        return $data;
    }

    public function criar($data)
    {
        $this->db->insert($this->table, $data);        
    }


    public function editar($data, $id)
    {
        $this->db->update($this->table, $data, "id = '$id'"); 
    }
}