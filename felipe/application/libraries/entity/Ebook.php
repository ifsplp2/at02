<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebook
{
    private $id;
    private $titulo;    
    private $img;
    private $link;
    private $visualizacoes;
    private $alteracao;

    function __construct($id, $titulo, $img, $link, $visualizacoes, $alteracao)
    {
        $this->titulo = $titulo;
        $this->id = $id;
        $this->img = $img;
        $this->link = $link;
        $this->visualizacoes = $visualizacoes;
        $this->alteracao = $alteracao;
    }

    public function toArray()
    {
        $array = array(
            "id" => $this->id,
            "titulo" => $this->titulo,
            "img" => $this->img,
            "link" => $this->link
        );

        return $array;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getImg()
    {
        return $this->img;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getVisualizacoes()
    {
        return $this->visualizacoes;
    }

    public function getAlteracao()
    {
        return $this->alteracao;
    }

    public function zerarVisualizacoes()
    {
        $this->visualizacoes = 0;
    }
}