<?php
defined('BASEPATH') OR exit('No direct script acess allowed');
include APPPATH.'libraries/util/DbContext.php';
include APPPATH.'libraries/entity/Ebook.php';

class ContadorModel extends CI_Model
{
    private $db;

    function __construct()
    {
        $this->db = new DbContext("ebook");
    }

    public function getAll()
    {
        $result = $this->db->getAll();
      
        foreach($result as $file)
        {
            $ebook = new Ebook("", $file['titulo'], "", "", $file['visualizacoes'], $file['alteracao']);
            $ebookList[] = $ebook;    
        }

        return $ebookList;    
    }

    public function totalAcessos()
    {
        $list = $this->getAll();
        $total = 0;

        foreach($list as $ebook)
        {
            $total += $ebook->getVisualizacoes();
        }

        return $total;
    }

    public function getFromLink($link)
    {
        return $this->db->getFrom("link", $link);
    }

    public function updateContador($link)
    {
        $link .= '?usp=drivesdk';
        $book = $this->getFromLink($link);
        $book[0]['visualizacoes']++;

        $this->db->editar($book[0], $book[0]['id']);
    }
}