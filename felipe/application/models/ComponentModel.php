<?php
defined('BASEPATH') OR exit('No direct script acess allowed');
include APPPATH.'libraries/component/eBookCard.php';
include APPPATH.'libraries/component/bibliotecaCarousel.php';
include APPPATH.'libraries/component/table.php';

class ComponentModel extends CI_Model
{
    public function ebookCard($ebook, $qntd)
    {
        $html='';

        for($i=0; $i < $qntd; $i++)
        {
            $card = new eBookCard($ebook[$i]);
            $html .= $card->getHTML();
        }
        
        return $html;
    }

    public function carousel($img1, $img2, $img3)
    {
        $carousel = new bibliotecaCarousel($img1, $img2, $img3);

        return $carousel->getHTML();
    }

    public function table($ebooks)
    {
        foreach($ebooks as $ebook)
        {
            if(is_null($ebook->getVisualizacoes()))
            {
                $ebook->zerarVisualizacoes();
            }    
        }

        $table = new table($ebooks);

        return $table->getHTML();
    }
}