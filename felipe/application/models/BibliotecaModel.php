<?php
defined('BASEPATH') OR exit('No direct script acess allowed');
include_once APPPATH.'libraries/entity/ebook.php';
include_once APPPATH.'libraries/util/GoogleDriveAPI.php';
include_once APPPATH.'libraries/util/DbContext.php';

class BibliotecaModel extends CI_Model
{
    private $gdApi;
    private $db;

    function __construct()
    {
        $this->gdApi = new GoogleDriveAPI();
        $this->db = new DbContext("ebook");
    }

    function getAll()
    {
        $ebookList;
        $result = $this->gdApi->getAll();

        foreach($result->getFiles() as $file)
        {
            $ebook = new Ebook($file->getId(), $file->getName(), $file->getThumbnailLink(), $file->getWebViewLink(),0,0);
            
            $ebookList[] = $ebook;            
        }

        return $ebookList;
    }

    function openFile($fileId)
    {
        return $this->gdApi->openFile($fileId);
    }

    function updateDatabase()
    {
        $googleBooks = $this->getAll();

        foreach($googleBooks as $book)
        {
            $data = $this->db->getFrom("id", $book->getId());          
            
            if(!isset($data))
            {                
                $this->db->criar($book->toArray());
            }
        }
    }
}